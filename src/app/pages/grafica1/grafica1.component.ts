import { Component } from '@angular/core';
import {Label} from "ng2-charts";

@Component({
  selector: 'app-grafica1',
  templateUrl: './grafica1.component.html',
  styles: [
  ]
})
export class Grafica1Component{

   labels1 = [ 'A', 'B', 'C' ];
   labels2 = [ 'D', 'E', 'F' ];
   labels3 = [ 'G', 'H', 'I' ];
   labels4 = [ 'J', 'K', 'L' ];

   data1 = [ 350, 450, 100 ];
   data2 = [ 50, 150, 200  ];
   data3 = [ 150, 250, 100 ];
   data4 = [ 250, 150, 300 ];

}
