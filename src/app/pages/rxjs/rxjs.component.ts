import {Component, OnDestroy} from '@angular/core';
import {filter, interval, map, Observable, retry, Subscription, take} from "rxjs";

@Component({
    selector: 'app-rxjs',
    templateUrl: './rxjs.component.html',
    styles: []
})
export class RxjsComponent implements OnDestroy {

    public intervalSubs: Subscription;

    constructor() {
        this.intervalSubs = this.retornaIntervalo().subscribe(console.log);
    }

    ngOnDestroy(): void {
        this.intervalSubs.unsubscribe();
    }

    retornaIntervalo(): Observable<number> {
        // Importa el orden de los opeardores del RxJs, en cascada
        return interval(500)
            .pipe(
                // take(20), // toma un limite de 4
                map(value => value + 1),   // y transforma esa data en otra
                filter(value => value % 2 == 0)
            );
    }
}