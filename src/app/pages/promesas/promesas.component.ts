import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-promesas',
    templateUrl: './promesas.component.html',
    styles: []
})
export class PromesasComponent implements OnInit {

    constructor() {
    }

    ngOnInit(): void {

        this.getUsuarios().then(usuarios => {
            console.log(usuarios)
        });


        const promesa = new Promise((resolve, reject) => {

            let bandera = false;

            if (bandera) {
                resolve('Hola mundo');
            }
            {
                reject('Algo salio mal');
            }

        });

        promesa
            .then((mensaje) => {
                console.log(mensaje);
            })
            .catch((err) => {
                console.log('Error en la promesa ', err);
            })

        console.log('Fin del Init');
    }

    getUsuarios() {

        return new Promise((resolve) => {
            fetch('https://reqres.in/api/users')
                .then(resp => resp.json())
                .then(body => resolve(body.data));

        });

    }

}
