import {Component, Input, OnInit} from '@angular/core';
import {Color, Label, MultiDataSet} from "ng2-charts";
import {ChartType} from "chart.js";

@Component({
  selector: 'app-dona',
  templateUrl: './dona.component.html',
  styles: [
  ]
})
export class DonaComponent {

  @Input() title: string = "Title default";
  @Input('labels') doughnutChartLabels: Label[] = [];
  @Input('data') doughnutChartData: number[] = [];

  public doughnutChartType: ChartType = 'doughnut';

  public colors: Color[] = [
    {backgroundColor: ['#9E120E','#FF5800','#FFB414']}
  ]

}
