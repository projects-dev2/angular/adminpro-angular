import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-incrementador',
  templateUrl: './incrementador.component.html',
  styles: [
  ]
})
export class IncrementadorComponent{

  @Input() progreso : number = 50;
  @Input() nameClase : string ='';

  @Output() incrementar : EventEmitter<number> = new EventEmitter<number>();

  get getPorcentaje(){
    return this.progreso;
  }

  cambiarValor(valor : number = this.progreso){

    console.log(this.progreso)
    console.log(valor)

    if(this.progreso >= 100 && valor >=0){
      this.progreso = 100;
      this.incrementar.emit(this.getPorcentaje);
      return;
    }

    if(this.progreso <= 0 && valor < 0){
      this.progreso = 0;
      this.incrementar.emit(this.getPorcentaje);
      return;
    }

    this.progreso = this.progreso + valor;
    this.incrementar.emit(this.getPorcentaje);

  }

  onChange(nuevoValor:number){

    if (nuevoValor >= 100)                        this.progreso = 100;
    if (nuevoValor <= 0)                          this.progreso = 0;
    if (nuevoValor >= 0 && nuevoValor <= 100)     this.progreso = 0;

    this.incrementar.emit(nuevoValor);
  }

}
