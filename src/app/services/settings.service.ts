import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class SettingsService {

    public linkTheme = document.querySelector('#theme')!;

    constructor() {
    }

    getThemeLocalStorage() {
        const themeTemp = localStorage.getItem('theme') || "./assets/css/colors/default-dark.css"
        this.linkTheme.setAttribute('href', themeTemp);
    }

    changeTheme(theme: string) {
        const url = `./assets/css/colors/${theme}.css`;
        this.linkTheme.setAttribute('href', url);
        localStorage.setItem('theme', url);
        this.checkCurrentTheme();
    }

    checkCurrentTheme() {
        const links: any[] | NodeListOf<Element> = document.querySelectorAll('.selector')!;
        links.forEach(element => {
            element.classList.remove('working');
            const buttonThemeUrl = `./assets/css/colors/${element.getAttribute('data-theme')}.css`;
            const buttonThemeCurrent = this.linkTheme.getAttribute('href');
            if (buttonThemeUrl === buttonThemeCurrent) {
                element.classList.add('working');
            }
        });
    }

}