import {Component, OnDestroy} from '@angular/core';
import {ActivationEnd, Router} from "@angular/router";
import {filter, map, Subscription} from "rxjs";

@Component({
    selector: 'app-breadcrumbs',
    templateUrl: './breadcrumbs.component.html',
    styles: []
})
export class BreadcrumbsComponent implements OnDestroy {

    public titulo: string = '';
    public tituloSubs$: Subscription;

    constructor(private router: Router) {
        this.tituloSubs$ = this.getArgumentosRuta();
    }

    ngOnDestroy(): void {
        this.tituloSubs$.unsubscribe();
    }

    getArgumentosRuta() {
        return this.router.events
            .pipe(
                filter((event): event is ActivationEnd => event instanceof ActivationEnd),
                filter((event: ActivationEnd) => event.snapshot.firstChild === null),
                map((event: ActivationEnd) => event.snapshot.data)
            )
            .subscribe(({titulo}) => {
                this.titulo = titulo;
                document.title = `AdminPro ${titulo}`;
            })
    }

    // filter(event => event instanceof ActivationEnd && event.snapshot.routeConfig?.path !== 'dashboard'),

    getArgumentosRutaOpcion2() {
        return this.router.events
            .pipe(
                filter((event: any) => event instanceof ActivationEnd),
                filter((event: ActivationEnd) => event.snapshot.firstChild === null),
                map((event: ActivationEnd) => event.snapshot.data)
            ).subscribe((event) => {
                console.log(event)
            })
    }

}